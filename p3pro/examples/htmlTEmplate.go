package main

import (
    "database/sql"
    "fmt"
    "html/template"
    "log"

    _ "github.com/go-sql-driver/mysql"
)

type User struct {
    Name string
    Age  int
}

func main() {
    db, err := sql.Open("mysql", "user:password@tcp(127.0.0.1:3306)/database")
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()

    rows, err := db.Query("SELECT name, age FROM users WHERE age > ?", 18)
    if err != nil {
        log.Fatal(err)
    }
    defer rows.Close()

    var users []User
    for rows.Next() {
        var user User
        if err := rows.Scan(&user.Name, &user.Age); err != nil {
            log.Fatal(err)
        }
        users = append(users, user)
    }
    if err := rows.Err(); err != nil {
        log.Fatal(err)
    }

    tmpl, err := template.ParseFiles("template.html")
    if err != nil {
        log.Fatal(err)
    }

    if err := tmpl.Execute(os.Stdout, users); err != nil {
        log.Fatal(err)
    }
}




/*

<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Users</title>
</head>
<body>
  <h1>Users</h1>
  <ul>
  {{range .}}
      <li>{{.Name}} ({{.Age}})</li>
  {{end}}
  </ul>
</body>
</html>


*/
