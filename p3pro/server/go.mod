module server

go 1.15

require (
	github.com/go-sql-driver/mysql v1.7.1
	golang.org/x/crypto v0.12.0
)
