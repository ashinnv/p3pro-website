package main
import(
	"fmt"
	"log"
	"sync"
	"strings"
	"net/http"
	"encoding/json"
	"database/sql"
	"golang.org/x/crypto/bcrypt"

	_"github.com/go-sql-driver/mysql"
)

/*

NOTE: I'm using functions returned from functions here to add inputs to the handle funcs. Some of these don't
have inputs, but I'm still using that techcnique for the sake of consistency. 

*/

type HandleFunc func(http.ResponseWriter, *http.Request)

type authCon struct{
	Payload Page
	Uname string
	Upass string
}

//Recieve submitted text and integrate into the website
func HandleTextRec(opts Convar) func(w http.ResponseWriter, r *http.Request){

	oChan := make(chan Page)
	go integratePage(oChan, opts)

	dec := authCon{}

	return func(w http.ResponseWriter, r *http.Request){

		if decErr := json.NewDecoder(r.Body).Decode(&dec); decErr != nil{
			fmt.Println("Decerr in textRec: ", decErr)
		}

		if fail := checkDec(dec, opts); fail == nil{
			oChan <- dec.Payload
		}else{
			fmt.Println("dec failed checkDec: ", fail)
		}


	}
}

func checkDec(pkg authCon, convars Convar) error {

	var passHashRec string

	//constring := fmt.Sprintf("%s:%s@%s(localhost):3306/%s", convars.Uname, convars.Upass, convars.Proto, convars.Datab)
	queryString := fmt.Sprintf("SELECT PassHash FROM Logins WHERE Uname = %s", pkg.Uname)

	if con/*,conerr*/ := getDbCon(convars)

	gott, quErr := con.Query(queryString)
	if quErr != nil{
		fmt.Println("Error in checkDec: ", quErr)
	}

	//We're just querying one row. Why did you do this?
	for gott.Next(){
		if scanerr := gott.Scan(&passHashRec); scanerr != nil{
			fmt.Println("Scan error in checkDec: ", checkDec)
		}

		if err := bcrypt.CompareHashAndPassword([]byte(passHashRec), []byte(pkg.Upass)); err != nil{
			fmt.Println(err) // nil means it's a match
			return err
		}
	}

	return nil
}

//Pull in a Page and integrate into the database
func integratePage(inchan chan Page, convars Convar)error{
/*
	constring := fmt.Sprintf("%s:%s@%s(localhost):3306/%s", convars.Uname, convars.Upass, convars.Proto, convars.Datab)	

	con, conerr := sql.Open("mysql", constring)
	if conerr != nil{
		log.Fatal("Couldnt' log into database: ", conerr)
	}
*/

	con, conErr := getDbCon(convars)
	stmt, err := con.Prepare("INSERT INTO Pages(Id, Title, Style, Url, Sections) VALUES(?, ?, ?, ?, ?)")
    if err != nil {
        log.Fatal("Could not prepare statement: ", err)
    }
    defer stmt.Close()

	for p := range inchan{
		// Convert the Sections slice to a JSON string
		sections, err := json.Marshal(p.Sections)
		if err != nil {
			log.Fatal("Could not marshal sections: ", err)
		}

		// Execute the statement with the Page fields
		_, err = stmt.Exec(p.Id, p.Title, p.Style, p.Url, sections)
		if err != nil {
			//log.Fatal("Could not execute statement: ", err)
			return err
		}

		// Print a success message
		fmt.Println("Successfully inserted page into database")

	}

	return nil

}

func accountHandler(convars Convar) func(w http.ResponseWriter, r *http.Request){
	return func(w http.ResponseWriter, r *http.Request){

		nacc := NewAccount{}
		if decErr := json.NewDecoder(r.Body).Decode(&nacc); decErr != nil{
			fmt.Println("Error in account handler: ", decErr)
		}

		if accErr := createAccount(nacc.Uname, nacc.Upass, convars); accErr != nil{
			fmt.Println("Error in account handler create account: ", accErr)
		}

	}
}


//input password and username and create that account in the DB
func createAccount(uname string, upass string, convars Convar)error{

	passHash, hashErr := bcrypt.GenerateFromPassword([]byte(upass), bcrypt.DefaultCost)
	if hashErr != nil{
		return hashErr
	}
/*
	constring := fmt.Sprintf("%s:%s@%s(localhost):3306/%s", convars.Uname, convars.Upass, convars.Proto, convars.Datab)
	dbCon, conerr := sql.Open("mysql",constring)
	if conerr != nil{
		return conerr
	}
*/

	if dbCon, conErr := getDbCon(convars); conErr != nil{
		fmt.Println("FAILED TO GET DBCON: ", conErr)
	}else{

	stmt, err := dbCon.Prepare("INSERT INTO Logins(Uname, PassHash) VALUES(?, ?)")
    if err != nil {
        log.Fatal("Could not prepare statement: ", err)
    }
    defer stmt.Close()

	if _,execErr := stmt.Exec(uname, passHash); execErr != nil{
		return execErr
	}
	
	return nil
}
}


func HandleText(stopper sync.WaitGroup, uname string, upass string) func(w http.ResponseWriter, r *http.Request){
	dbCon := getDbCon(uname, upass)
	return func(w http.ResponseWriter, r *http.Request) {
		// Get the last wordword from the URL path
		urlPath := r.URL.Path
		urlParts := strings.Split(urlPath, "/")
		lastWord := urlParts[len(urlParts)-1]

		// Query the database for the associated blob
		var pageTmp Page
		err := dbCon.QueryRow("SELECT Page FROM Pages WHERE Url = ?", lastWord).Scan(&pageTmp.Id, &pageTmp.Title, &pageTmp.Style, &pageTmp.Url, &pageTmp.Sections)
		if err != nil {
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return 
		}

		// Serve the blob as a response
		w.Header().Set("Content-Type", "application/octet-stream")

		compiled := buildPage(pageTmp)
		w.Write([]byte(compiled))
	}
}

//Serve blob-like data; images, wasm, etc...
func serveBlob(stopper sync.WaitGroup, uname string, upass string) func(w http.ResponseWriter, r *http.Request){

	dbCon := getDbCon(uname, upass)

	return func(w http.ResponseWriter, r *http.Request) {
		// Get the last word from the URL path
		urlPath := r.URL.Path
		urlParts := strings.Split(urlPath, "/")
		lastWord := urlParts[len(urlParts)-1]

		// Query the database for the associated blob
		var blob []byte
		err := dbCon.QueryRow("SELECT Data FROM BlobBox WHERE Url = ?", lastWord).Scan(&blob)
		if err != nil {
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return 
		}

		// Serve the blob as a response
		w.Header().Set("Content-Type", "application/octet-stream")
		w.Write(blob)
	}
}

func ServeRoot(rootLoc string) func(w http.ResponseWriter, r *http.Request){
	return func(w http.ResponseWriter, r *http.Request){
		rootFile, rfErr := os.ReadFile(rootLoc)
		if rfErr != nil{
			log.Fatal(rfErr)
		}

		w.Header().Set("Content-Type", "text/html")
		w.Write(rootFile)

	}
}

//Serve vanilla, non-jquery javascript
func ServeVanilla(jsLoc string) func(http.ResponseWriter, r *http.Request){
	return func(w http.ResponseWriter, r *http.Request){
		if vanifile, vFileErr := os.ReadFile(jsLoc); vFileErr != nil{
			fmt.Println("Error with reading vanilla js file: ", vFileErr)
		}else{
			w.Header().Set("Content-Type","application/javascript")
			w.Write(vanifile)
		}
	}
}

func ServeJquery(jqLoc string) func(http.ResponseWriter, r *http.Request){
	return func(w http.ResponseWriter, r *http.Request){
		if jqDat, jqErr := os.ReadFile(jqLoc); jqErr != nil{
			log.Fatal("jqErr: ", jqErr)
		}else{
			w.Header().Set("Content-Type", "application/javascript")
			w.Write(jqDat)
		}
	}
}

func ServeStyle(stylLoc string) func(http.ResponseWriter, r *http.Request){
	return func(w http.ResponseWriter, r *http.Request){
		if cssDat, cssErr := os.ReadFile(stylLoc); cssErr != nil{
			log.Fatal("css err: ", cssErr)
		}else{
			w.Header().Set("Content-Type", "text/css")
			w.Write(cssDat)
		}
	}
}




/*
Example:

import (
    "fmt"
    "golang.org/x/crypto/bcrypt"
)

func main() {
    password := "password123"
    hash, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)

    err := bcrypt.CompareHashAndPassword(hash, []byte(password))
    fmt.Println(err) // nil means it's a match
}



*/