package main

type Page struct{
	Id       uint64   //Indexable id of this page
	Title    string   //Title of page
	Style    uint64   //Links to style info (overrides global style)
	Url      string   //URL to this page
	Sections []uint64 //Links to sections in this page
}

type Section struct{
	Id    uint64
	Title string
	Body  string
}


type Convar struct{
	Uname string
	Upass string
	Proto string //Protocol for connecting to the db
	Datab string //Name of database
}

type NewAccount struct{
	Uname string
	Upass string
}


//CREATE TABLE Pages (     Id INT UNSIGNED PRIMARY KEY,     Title VARCHAR(255), Style INT UNSIGNED,     Url VARCHAR(255),     Sections JSON );